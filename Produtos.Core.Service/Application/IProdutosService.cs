﻿using Produtos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Produtos.Core.Service.Application
{
    public interface IProdutosService
    {
        IEnumerable<Produto> PegaListaProdutos();
        IEnumerable<Produto> PegaProdutoPorId(int produtoId);
        void InserirProduto(Produto produto);
    }
}
