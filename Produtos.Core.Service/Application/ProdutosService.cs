﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using Produtos.Core.Service.Data;
using Produtos.Core.Service.Models;

namespace Produtos.Core.Service.Application
{
    public class ProdutosService : IProdutosService
    {
        private readonly ProdutosContext context;

        public ProdutosService()
        {
            context = new ProdutosContext();
        }

        public IEnumerable<Produto> PegaListaProdutos()
        {
            var produtos = context.Produto.Find(_ => true).ToList();
            return produtos;
        }

        public IEnumerable<Produto> PegaProdutoPorId(int produtoId)
        {
            var produto = context.Produto.Find(x => x.produtoId  == produtoId).ToList();

            return produto;
        }

        public void InserirProduto(Produto produto)
        {
            try
            {
                context.Produto.InsertOne(produto);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível inserir o produto.", ex);
            }
        }
    }
}
