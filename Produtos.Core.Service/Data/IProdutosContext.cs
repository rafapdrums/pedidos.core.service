﻿using MongoDB.Driver;
using Produtos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Produtos.Core.Service.Data
{
    public interface IProdutosContext
    {
        IMongoCollection<Produto> Produto { get; }
    }
}
