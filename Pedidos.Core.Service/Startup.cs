﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Pedidos.Core.Service.Applications;
using Pedidos.Core.Service.Data;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Pedidos.Core.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            AuthConfigure(services);
            ConfigureDependencyInjection(services);

            ILoggerFactory loggerFactory = new LoggerFactory();
            loggerFactory.AddDebug();

            services.AddSingleton<ILoggerFactory>(loggerFactory);
                        
            services.AddSwaggerGen(c =>
                   {
                       c.SwaggerDoc("v1", new Info { Title = "Pedidos.Core.Service", Description = "Core API" });

                       var xmlPath = System.AppDomain.CurrentDomain.BaseDirectory + @"Pedidos.Core.Service.xml";
                       c.IncludeXmlComments(xmlPath);
                   }
             );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI( c =>
                    {
                        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Core API");
                    }
                );
        }

        public void AuthConfigure(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = "iguatemi",
                        ValidAudience = "iguatemi",
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["SecurityKey"]))
                    };

                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            Console.WriteLine("Token inválido... " + context.Exception.Message);
                            return Task.CompletedTask;
                        },
                        OnTokenValidated = context =>
                        {
                            Console.WriteLine("Token válido... " + context.SecurityToken);
                            return Task.CompletedTask;
                        }
                    };
                });
        }

        public void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<IPedidosContext, PedidosContext>();
            services.AddTransient<IClientesContext, ClientesContext>();
            services.AddTransient<IPedidosService, PedidosService>();
            services.AddTransient<IClientesService, ClienteService>();
        }
    }
}
