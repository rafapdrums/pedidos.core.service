﻿using MongoDB.Driver;
using Pedidos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Data
{
    public class ClientesContext : IClientesContext
    {
        private readonly IMongoDatabase database;

        public ClientesContext()
        {
            try
            {
                database = new MongoClient("mongodb://localhost:27017").GetDatabase("iguatemi");
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível se conectar com o servidor.", ex);
            }
        }

        public IMongoCollection<Cliente> Cliente
        {
            get
            {
                {
                    return database.GetCollection<Cliente>("clientes");
                }
            }
        }
    }
}

