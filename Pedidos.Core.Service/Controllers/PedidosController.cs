﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Core.Service.Applications;
using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;

namespace Pedidos.Core.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private IPedidosService pedidosService;
        
        public PedidosController(IPedidosService _pedidosService)
        {
            pedidosService = _pedidosService;
        }

        //TODO fazer com Task e repository async
        [HttpGet]
        [Route("PegaListaPedidos")]
        public IEnumerable<Pedido> PegaListaPedidos()
        {
            return pedidosService.PegaListaPedidos();
        }

        //TODO fazer com Task e repository async
        [HttpGet]
        [Route("PegaPedido")]
        public IEnumerable<Pedido> PegaPedidoPorId([FromQuery] int pedidoId)
        {
            return pedidosService.PegaPedidoPorId(pedidoId);
        }

        //TODO fazer com Task e repository async
        [HttpPost]
        [Route("InserirPedido")]
        public IActionResult InserirPedido([FromBody] PedidoDto pedido)
        {
            if (pedidosService.InserirPedido(pedido))
            {
                return Ok();
            }
            return NotFound();
        }

        //TODO fazer com Task e repository async await
        [HttpDelete]
        [Route("DeletarPedido")]
        public Task DeletarPedido([FromQuery] int pedidoId)
        {
            return pedidosService.DeletarPedido(pedidoId);
        }

        //TODO fazer com Task e repository async
        [HttpPut]
        [Route("AtualizarPedido")]
        public IActionResult AtualizarPedido(int pedidoId, [FromBody] PedidoDto pedido)
        {
            if (pedidosService.AtualizarPedido(pedidoId, pedido))
            {
                return Ok();
            }
            return NotFound();
        }
    }
}
