﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models.Dto
{
    public class ClienteDto
    {
        public string nome { get; set; }
        public string cpf { get; set; }
        public string endereco { get; set; }
    }
}
