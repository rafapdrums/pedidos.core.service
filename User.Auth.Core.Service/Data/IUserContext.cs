﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Data
{
    interface IUserContext
    {
        IMongoCollection<Usuario> Usuario { get; }
    }
}
