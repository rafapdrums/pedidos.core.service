﻿using Produtos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Produtos.Core.Service.Application
{
    public interface IProdutosService
    {
        IEnumerable<Produto> PegaListaProdutos(Produto produto);
        IEnumerable<Produto> PegaProduto(Produto produto);
        void InserirProduto(Produto produto);
    }
}
