﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Produtos.Core.Service.Application;
using Produtos.Core.Service.Models;

namespace Produtos.Core.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize()]
    public class ProdutosController : ControllerBase
    {
        private IProdutosService produtosService;

        public ProdutosController(IProdutosService _produtosService)
        {
            produtosService = _produtosService;
        }

        // GET: api/Produtos
        [HttpGet]
        public IEnumerable<Produto> PegaListaProdutos([FromBody] Produto produto)
        {
            return produtosService.PegaListaProdutos(produto);
        }

        [HttpGet]
        public IEnumerable<Produto> PegaProduto([FromBody] Produto produto)
        {
            return produtosService.PegaProdutos(produto);
        }

        [HttpPost]
        public void InserirProduto([FromBody] Produto produto)
        {

        }
    }
}
