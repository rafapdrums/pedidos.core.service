﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Produtos.Core.Service.Models;

namespace Produtos.Core.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize()]
    public class ProdutosController : ControllerBase
    {
        // GET: api/Produtos
        [HttpGet]
        public IEnumerable<Produto> PegaListaProdutos([FromBody] Produto produto)
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        public IEnumerable<Produto> PegaProduto([FromBody] Produto produto)
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        public void InserirProduto([FromBody] Produto produto)
        {

        }
    }
}
