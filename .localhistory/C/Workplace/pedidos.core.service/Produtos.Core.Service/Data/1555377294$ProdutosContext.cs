﻿using MongoDB.Driver;
using Produtos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Produtos.Core.Service.Data
{
    public class ProdutosContext
    {
        private readonly IMongoDatabase database;

        public ProdutosContext()
        {
            try
            {
                database = new MongoClient("mongodb://localhost:27017").GetDatabase("iguatemi");
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível se conectar com o servidor.", ex);
            }
        }

        public IMongoCollection<Produto> Produto
        {
            get
            {
                return database.GetCollection<Produto>("pedidos");
            }
        }
    }
}
