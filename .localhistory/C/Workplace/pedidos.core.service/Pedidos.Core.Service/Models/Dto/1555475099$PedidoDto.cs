﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models.Dto
{
    public class PedidoDto
    {
        public Array produtos { get; set; }
        public ClienteDto cliente { get; set; }
        public decimal valorFrete { get; set; }
        public decimal valorTotalPedido { get; set; }

    }
}
