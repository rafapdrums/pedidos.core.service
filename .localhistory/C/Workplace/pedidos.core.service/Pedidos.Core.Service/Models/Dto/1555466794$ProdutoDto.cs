﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models.Dto
{
    public class ProdutoDto
    {
        public string nome { get; set; }
        public string marca { get; set; }
        public string tipo { get; set; }
        public string tamanho { get; set; }
        public int quantidade { get; set; }
        public decimal valor { get; set; }
    }
}
