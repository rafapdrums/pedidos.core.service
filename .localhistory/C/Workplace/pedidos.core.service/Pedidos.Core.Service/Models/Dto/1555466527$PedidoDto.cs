﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models.Dto
{
    public class PedidoDto
    {
        public List<Produto> produtos { get; set; }
        public List<Cliente> cliente { get; set; }
        public decimal valorFrete { get; set; }
        public decimal valorTotalPedido { get; set; }

    }
}
