﻿using MongoDB.Bson.Serialization.Attributes;
using Pedidos.Core.Service.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models
{
    public class Pedido
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string _id { get; set; }

        public int pedidoId { get; set; }
        public List<Produto> produtos { get; set; }
        public Cliente cliente { get; set; }
        public decimal valorFrete { get; set; }
        public decimal valorTotalPedido { get; set; }

        public Pedido()
        {

        }

        public Pedido(PedidoDto pedidoInput)
        {
            foreach (var produto in pedidoInput.produtos)
            {
                produtos.Add(new Produto(produto));
            }

            cliente = new Cliente(pedidoInput.cliente);
            valorFrete = pedidoInput.valorFrete;
            valorTotalPedido = pedidoInput.valorTotalPedido;
        }

        public Pedido(int novoPedidoId, PedidoDto pedidoInput)
        {
            pedidoId = novoPedidoId;

            foreach (var produto in pedidoInput.produtos)
            {
                produtos.Add(new Produto(produto));
            }

            cliente = new Cliente(pedidoInput.cliente);
            valorFrete = pedidoInput.valorFrete;
            valorTotalPedido = pedidoInput.valorTotalPedido;
        }
    }
}
