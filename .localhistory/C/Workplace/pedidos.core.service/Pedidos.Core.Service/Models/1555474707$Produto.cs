﻿using MongoDB.Bson.Serialization.Attributes;
using Pedidos.Core.Service.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models
{
    public class Produto
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string _id { get; set; }

        public int produtoId { get; set; }
        public string nome { get; set; }
        public string marca { get; set; }
        public string tipo { get; set; }
        public string tamanho { get; set; }
        public int estoque { get; set; }
        public decimal valor { get; set; }

        public Produto()
        {

        }

        public Produto(ProdutoDto produtoInput)
        {
            nome = produtoInput.nome;
            marca = produtoInput.marca;
            tipo = produtoInput.tipo;
            tamanho = produtoInput.tamanho;
            estoque = produtoInput.estoque;
        }
    }
}
