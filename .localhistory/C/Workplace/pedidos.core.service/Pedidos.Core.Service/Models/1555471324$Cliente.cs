﻿using MongoDB.Bson.Serialization.Attributes;
using Pedidos.Core.Service.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Models
{
    public class Cliente
    {
        [BsonId]
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string _id { get; set; }

        public int clienteId { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string endereco { get; set; }

        public Cliente()
        {

        }

        public Cliente(ClienteDto clienteInput)
        {
            nome = clienteInput.nome;
            cpf = clienteInput.cpf;
            endereco = clienteInput.endereco;
        }
    }
}
