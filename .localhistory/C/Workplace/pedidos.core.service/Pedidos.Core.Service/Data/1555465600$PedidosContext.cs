﻿using MongoDB.Driver;
using Pedidos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Data
{
    public class PedidosContext : IPedidosContext
    {
        private readonly IMongoDatabase database;

        public PedidosContext()
        {
            try
            {
                database = new MongoClient("mongodb://localhost:27017").GetDatabase("iguatemi");
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível se conectar com o servidor.", ex);
            }
        }

        public IMongoCollection<Pedido> Pedido
        {
            get
            {
                return database.GetCollection<Pedido>("Pedidos");
            }
        }
    }
}
