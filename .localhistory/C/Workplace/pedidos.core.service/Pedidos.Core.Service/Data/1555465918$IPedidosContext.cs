﻿using MongoDB.Driver;
using Pedidos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Data
{
    public interface IPedidosContext
    {
        IMongoCollection<PedidoDto> Pedido { get; }
    }
}
