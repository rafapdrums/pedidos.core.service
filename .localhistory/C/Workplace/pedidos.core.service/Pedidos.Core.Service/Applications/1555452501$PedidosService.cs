﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using Pedidos.Core.Service.Data;
using Pedidos.Core.Service.Models;

namespace Pedidos.Core.Service.Applications
{
    public class PedidosService : IPedidosService
    {
        IPedidosContext pedidosContext;

        public PedidosService(IPedidosContext _pedidosContext)
        {
            pedidosContext = _pedidosContext;
        }

        public void DeletarPedido(int pedidoId)
        {
            try
            {
                pedidosContext.Pedido.DeleteOneAsync(Builders<Pedido>.Filter.Eq("pedidoId", pedidoId));
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível deletar o pedido.", ex);
                throw;
            }
        }

        public void InserirPedido(Pedido pedido)
        {
            try
            {
                pedidosContext.Pedido.InsertOne(pedido);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível inserir o pedido.", ex);
            }
        }

        public IEnumerable<Pedido> PegaListaPedidos()
        {
            var pedidos = pedidosContext.Pedido.Find(_ => true).ToList();
            return pedidos;
        }

        public IEnumerable<Pedido> PegaPedidoPorId(int pedidoId)
        {
            throw new NotImplementedException();
        }
    }
}
