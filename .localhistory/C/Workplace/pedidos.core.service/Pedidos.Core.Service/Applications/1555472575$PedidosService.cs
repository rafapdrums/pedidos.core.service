﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using Pedidos.Core.Service.Data;
using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;

namespace Pedidos.Core.Service.Applications
{
    public class PedidosService : IPedidosService
    {
        IPedidosContext pedidosContext;

        public PedidosService(IPedidosContext _pedidosContext)
        {
            pedidosContext = _pedidosContext;
        }

        public bool AtualizarPedido(int pedidoId, PedidoDto pedidoInput)
        {
                Pedido pedido = new Pedido(pedidoInput);

                var updatePedido = pedidosContext.Pedido.ReplaceOneAsync(zz => zz.pedidoId == pedidoId, pedido);

                if (updatePedido.IsCompleted)
                {
                    return true;
                }
                return false;
        }

        public bool DeletarPedido(int pedidoId)
        {
            var pedidoDelete = pedidosContext.Pedido.DeleteOneAsync(Builders<Pedido>.Filter.Eq("pedidoId", pedidoId));

            if (pedidoDelete.IsCompleted)
            {
                return true;
            }
            return false;
        }

        public bool InserirPedido(PedidoDto pedidoInput)
        {
            int novoPedidoId = 0;
            var pedidosExistentes = pedidosContext.Pedido.Find(_ => true).ToList();

            foreach (var pedidoDisponivel in pedidosExistentes)
            {
                if (pedidoDisponivel.pedidoId > novoPedidoId)
                { 
                    novoPedidoId = pedidoDisponivel.pedidoId;
                }
            }

            Pedido pedido = new Pedido(novoPedidoId, pedidoInput);

            var pedidoInsert = pedidosContext.Pedido.InsertOneAsync(pedido);

            if (pedidoInsert.IsCompleted)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<Pedido> PegaListaPedidos()
        {
            var pedidos = pedidosContext.Pedido.Find(_ => true).ToList();
            return pedidos;
        }

        public IEnumerable<Pedido> PegaPedidoPorId(int pedidoId)
        {
            var pedido = pedidosContext.Pedido.Find(x => x.pedidoId == pedidoId).ToList();
            return pedido;
        }
    }
}
