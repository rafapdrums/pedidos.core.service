﻿using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Applications
{
    interface IClientesService
    {
        IEnumerable<Cliente> PegaListaCliente();
        IEnumerable<Cliente> PegaClientePorId(int pedidoId);
        bool InserirCliente(ClienteDto pedido);
        bool DeletarCliente(int clienteId);
        bool AtualizarCliente(int clienteId, ClienteDto pedido);
    }
}
