﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pedidos.Core.Service.Data;
using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;
using MongoDB.Driver;

namespace Pedidos.Core.Service.Applications
{
    public class ClienteService : IClientesService
    {
        IClientesContext clientesContext;

        public ClienteService(IClientesContext _clientesContext)
        {
            clientesContext = _clientesContext;
        }

        public bool AtualizarCliente(int clienteId, ClienteDto clienteInput)
        {
            Cliente cliente  = new Cliente(clienteInput);

            var updateCliente = clientesContext.Cliente.ReplaceOneAsync(zz => zz.clienteId == clienteId, cliente);

            if (updateCliente.IsCompleted)
            {
                return true;
            }
            return false;
        }

        public bool DeletarCliente(int clienteId)
        {
            var clienteDelete = clientesContext.Cliente.DeleteOneAsync(Builders<Cliente>.Filter.Eq("clienteId", clienteId));

            if (clienteDelete.IsCompleted)
            {
                return true;
            }
            return false;
        }

        public bool InserirCliente(ClienteDto clienteInput)
        {
            int novoClienteId = 0;
            var clientesExistentes = clientesContext.Cliente.Find(_ => true).ToList();

            foreach (var clienteDisponivel in clientesExistentes)
            {
                if (clienteDisponivel.clienteId > novoClienteId)
                {
                    novoClienteId = clienteDisponivel.clienteId;
                }
            }

            Cliente cliente = new Pedido(novoClienteId, clienteInput);

            var pedidoInsert = clientesContext.Cliente.InsertOneAsync(cliente);

            if (pedidoInsert.IsCompleted)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<Cliente> PegaClientePorId(int pedidoId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Cliente> PegaListaCliente()
        {
            throw new NotImplementedException();
        }
    }
}
