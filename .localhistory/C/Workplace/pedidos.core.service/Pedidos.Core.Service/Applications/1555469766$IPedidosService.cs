﻿using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Applications
{
    public interface IPedidosService
    {
        IEnumerable<Pedido> PegaListaPedidos();
        IEnumerable<Pedido> PegaPedidoPorId(int pedidoId);
        void InserirPedido(PedidoDto pedido);
        void DeletarPedido(int pedidoId);
        bool AtualizarPedido(int pedidoId, PedidoDto pedido);
    }
}
