﻿using Pedidos.Core.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.Core.Service.Applications
{
    public interface IPedidosService
    {
        IEnumerable<Pedido> PegaListaPedidos();
        IEnumerable<Pedido> PegaPedidoPorId(int pedidoId);
        void InserirPedido(Pedido pedido);
        void DeletarPedido(int pedidoId);
        bool AtualizarPedido(Pedido pedido);
    }
}
