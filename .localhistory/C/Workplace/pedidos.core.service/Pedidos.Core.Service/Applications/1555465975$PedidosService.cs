﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using Pedidos.Core.Service.Data;
using Pedidos.Core.Service.Models;

namespace Pedidos.Core.Service.Applications
{
    public class PedidosService : IPedidosService
    {
        IPedidosContext pedidosContext;

        public PedidosService(IPedidosContext _pedidosContext)
        {
            pedidosContext = _pedidosContext;
        }

        public bool AtualizarPedido(int pedidoId, PedidoDto pedido)
        {
            var updatePedido = pedidosContext.Pedido.ReplaceOneAsync(zz => zz.pedidoId == pedidoId, pedido);
            return true;
        }

        public void DeletarPedido(int pedidoId)
        {
            try
            {
                pedidosContext.Pedido.DeleteOneAsync(Builders<PedidoDto>.Filter.Eq("pedidoId", pedidoId));
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível deletar o pedido.", ex);
                throw;
            }
        }

        public void InserirPedido(PedidoDto pedido)
        {
            try
            {
                pedidosContext.Pedido.InsertOne(pedido);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível inserir o pedido.", ex);
            }
        }

        public IEnumerable<PedidoDto> PegaListaPedidos()
        {
            var pedidos = pedidosContext.Pedido.Find(_ => true).ToList();
            return pedidos;
        }

        public IEnumerable<PedidoDto> PegaPedidoPorId(int pedidoId)
        {
            var pedido = pedidosContext.Pedido.Find(x => x.pedidoId == pedidoId).ToList();
            return pedido;
        }
    }
}
