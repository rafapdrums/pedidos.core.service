﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Core.Service.Applications;

namespace Pedidos.Core.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private IPedidosService pedidosService;
        
        public PedidosController(IPedidosService _pedidosService)
        {
            pedidosService = _pedidosService;

        }


        [HttpGet]
        [Route("PegaListaProdutos")]
        public IEnumerable<Produto> PegaListaProdutos()
        {
            return produtosService.PegaListaProdutos();
        }

        [HttpGet]
        [Route("PegaProduto")]
        public IEnumerable<Produto> PegaProduto([FromQuery] int produtoId)
        {
            return produtosService.PegaProdutoPorId(produtoId);
        }

        [HttpPost]
        [Route("InserirProduto")]
        public void InserirProduto([FromBody] Produto produto)
        {
            produtosService.InserirProduto(produto);
        }
        //// GET: api/Pedidos
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/Pedidos/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/Pedidos
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/Pedidos/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
