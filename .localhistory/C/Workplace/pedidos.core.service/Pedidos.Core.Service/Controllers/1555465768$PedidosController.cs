﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Core.Service.Applications;
using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;

namespace Pedidos.Core.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private IPedidosService pedidosService;
        
        public PedidosController(IPedidosService _pedidosService)
        {
            pedidosService = _pedidosService;

        }

        //TODO fazer com Task e repo async
        [HttpGet]
        [Route("PegaListaPedidos")]
        public IEnumerable<Models.PedidoDto> PegaListaPedidos()
        {
            return pedidosService.PegaListaPedidos();
        }

        //TODO fazer com Task e repo async
        [HttpGet]
        [Route("PegaPedido")]
        public IEnumerable<Models.PedidoDto> PegaPedidoPorId([FromQuery] int pedidoId)
        {
            return pedidosService.PegaPedidoPorId(pedidoId);
        }

        //TODO fazer com Task e repo async
        [HttpPost]
        [Route("InserirPedido")]
        public void InserirPedido([FromBody] Models.Dto.PedidoDto pedido)
        {
            pedidosService.InserirPedido(pedido);
        }

        //TODO fazer com Task e repo async
        [HttpDelete]
        [Route("DeletarPedido")]
        public void DeletarPedido([FromQuery] int pedidoId)
        {
            pedidosService.DeletarPedido(pedidoId);
        }

        //TODO fazer com Task e repo async
        [HttpPut]
        [Route("AtualizarPedido")]
        public IActionResult AtualizarPedido(int pedidoId, [FromBody] Models.PedidoDto pedido)
        {
            if (!pedidosService.AtualizarPedido(pedidoId, pedido))
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
