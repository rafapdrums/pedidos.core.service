﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Core.Service.Applications;
using Pedidos.Core.Service.Models;
using Pedidos.Core.Service.Models.Dto;

namespace Pedidos.Core.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private IClientesService clientesService;

        public ClientesController(IClientesService _clientesService)
        {
            clientesService = _clientesService;
        }

        //TODO fazer com Task e repository async
        [HttpGet]
        [Route("PegaListaClientes")]
        public IEnumerable<Cliente> PegaListaClientes()
        {
            return clientesService.PegaListaCliente();
        }

        //TODO fazer com Task e repository async
        [HttpGet]
        [Route("PegaCliente")]
        public IEnumerable<Cliente> PegaClientePorId([FromQuery] int clienteId)
        {
            return clientesService.PegaClientePorId(clienteId);
        }

        //TODO fazer com Task e repository async
        [HttpPost]
        [Route("InserirCliente")]
        public IActionResult InserirCliente([FromBody] ClienteDto cliente)
        {
            if (clientesService.InserirCliente(cliente))
            {
                return Ok();
            }
            return NotFound();
        }

        //TODO fazer com Task e repository async
        [HttpDelete]
        [Route("DeletarCliente")]
        public IActionResult DeletarCliente([FromQuery] int clienteId)
        {
            if (clientesService.DeletarCliente(clienteId))
            {
                return Ok();
            }
            return NotFound();
        }

        //TODO fazer com Task e repository async
        [HttpPut]
        [Route("AtualizarCliente")]
        public IActionResult AtualizarCliente(int clienteId, [FromBody] ClienteDto cliente)
        {
            if (clientesService.AtualizarCliente(clienteId, cliente))
            {
                return Ok();
            }
            return NotFound();
        }
    }
}
