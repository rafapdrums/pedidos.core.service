﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Core.Service.Applications;
using Pedidos.Core.Service.Models;

namespace Pedidos.Core.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private IPedidosService pedidosService;
        
        public PedidosController(IPedidosService _pedidosService)
        {
            pedidosService = _pedidosService;

        }
        
        [HttpGet]
        [Route("PegaListaPedidos")]
        public IEnumerable<Pedido> PegaListaPedidos()
        {
            return pedidosService.PegaListaPedidos();
        }

        [HttpGet]
        [Route("PegaPedido")]
        public IEnumerable<Produto> PegaProduto([FromQuery] int pedidoId)
        {
            return pedidosService.PegaPedidoPorId(pedidoId);
        }

        [HttpPost]
        [Route("InserirPedido")]
        public void InserirPedido([FromBody] Pedido pedido)
        {
            pedidosService.InserirPedido(pedido);
        }

        [HttpDelete]
        [Route("DeletarPedido")]
        public void DeletarPedido([FromQuery] int pedidoId)
        {
            pedidosService.DeletarPedido(pedidoId);
        }
    }
}
