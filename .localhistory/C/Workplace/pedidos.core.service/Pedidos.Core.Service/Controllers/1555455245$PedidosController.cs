﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Core.Service.Applications;
using Pedidos.Core.Service.Models;

namespace Pedidos.Core.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private IPedidosService pedidosService;
        
        public PedidosController(IPedidosService _pedidosService)
        {
            pedidosService = _pedidosService;

        }
        
        //TODO fazer com ASYNC Task
        [HttpGet]
        [Route("PegaListaPedidos")]
        public IEnumerable<Pedido> PegaListaPedidos()
        {
            return pedidosService.PegaListaPedidos();
        }

        //TODO fazer com ASYNC Task
        [HttpGet]
        [Route("PegaPedido")]
        public IEnumerable<Pedido> PegaPedidoPorId([FromQuery] int pedidoId)
        {
            return pedidosService.PegaPedidoPorId(pedidoId);
        }

        //TODO fazer com ASYNC Task
        [HttpPost]
        [Route("InserirPedido")]
        public void InserirPedido([FromBody] Pedido pedido)
        {
            pedidosService.InserirPedido(pedido);
        }

        //TODO fazer com ASYNC Task
        [HttpDelete]
        [Route("DeletarPedido")]
        public void DeletarPedido([FromQuery] int pedidoId)
        {
            pedidosService.DeletarPedido(pedidoId);
        }

        //TODO fazer com ASYNC Task
        [HttpPut]
        [Route("AtualizarPedido")]
        public IActionResult AtualizarPedido(int pedidoId, [FromBody] Pedido pedido)
        {
            if (!pedidosService.AtualizarPedido(pedidoId, pedido))
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
