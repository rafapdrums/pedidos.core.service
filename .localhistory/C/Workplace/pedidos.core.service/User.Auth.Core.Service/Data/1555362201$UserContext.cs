﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Data
{
    public class UserContext
    {
        private readonly IMongoDatabase database;

        public UserContext()
        {
            database = new MongoClient("mongodb://localhost:27017").GetDatabase("iguatemi");
        }

        public IMongoCollection<Usuario> Usuario
        {
            get
            {
                return database.GetCollection<Usuario>("user");
            }
        }
    }
}
