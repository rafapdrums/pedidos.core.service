﻿using MongoDB.Driver;
using System;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Data
{
    public class UserContext
    {
        private readonly IMongoDatabase database;

        public UserContext()
        {
            try
            {
                database = new MongoClient("mongodb://localhost:27017").GetDatabase("iguatemi");
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível se conectar com o servidor.", ex);
            }
        }

        public IMongoCollection<Usuario> Usuario
        {
            get
            {
                return database.GetCollection<Usuario>("user");
            }
        }
    }
}
