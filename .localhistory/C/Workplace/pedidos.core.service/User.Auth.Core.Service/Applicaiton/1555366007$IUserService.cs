﻿using System.Collections.Generic;
using User.Auth.Core.Service.Controllers;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Applicaiton
{
    public interface IUserService
    {
        IEnumerable<Usuario> PegaUsuario(Usuario user);
        IActionResult<Usuario> ChecaUsuario(Usuario user);
        void InserirUsuario(Usuario user);
    }
}