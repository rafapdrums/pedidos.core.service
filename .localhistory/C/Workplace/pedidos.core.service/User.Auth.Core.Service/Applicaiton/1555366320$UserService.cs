﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Auth.Core.Service.Data;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Applicaiton
{
    public class UserService : IUserService
    {
        private readonly UserContext context;

        public IEnumerable<Usuario> ChecaUsuario(Usuario user)
        {
            var usuario = context.Usuario.Find(x => x.nome == user.nome && x.senha == user.senha).ToList();

            return usuario;
        }

        public void InserirUsuario(Usuario user)
        {
            try
            {
                context.Usuario.InsertOne(user);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível inserir o usuário.", ex);
            }
        }
    }
}
