﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Applicaiton
{
    public class TokenService : ITokenService
    {
        public IActionResult RequestToken(Usuario request)
        {
            if (request.nome == "teste" && request.senha == "teste")
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, request.nome),
                };

                var key = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));

                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                        issuer: "iguatemi",
                        audience: "iguatemi",
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(30),
                        signingCredentials: credentials);

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }
            return BadRequest("Credenciais inválidas...");
        }
    }
}
