﻿using System.Collections.Generic;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Applicaiton
{
    public interface IUserService
    {
        IEnumerable<Usuario> ChecaUsuario(Usuario user);
    }
}