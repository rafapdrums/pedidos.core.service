﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Applicaiton
{
    public interface IUserService
    {
        IEnumerable<Usuario> ChecaUsuario(Usuario user);
    }
}
