﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.Auth.Core.Service.Models
{
    public class Usuario
    {
        public int userId { get; set; }
        public string nome { get; set; }
        public string senha { get; set; }
    }
}
