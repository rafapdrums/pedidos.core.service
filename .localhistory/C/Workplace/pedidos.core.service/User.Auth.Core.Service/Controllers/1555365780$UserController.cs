﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using User.Auth.Core.Service.Applicaiton;
using User.Auth.Core.Service.Data;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService userService;

        public UserController(IUserService _userService)
        {
            userService = _userService;            
        }

        [HttpGet]
        public IEnumerable<Usuario> ChecaUsuario([FromBody] Usuario user)
        {
            //var usuario = context.Usuario.Find(x => x.nome == user.nome && x.senha == user.senha).ToList();
            return userService.ChecaUsuario(user);
        }

        [HttpPost]
        public void InserirUsuario([FromBody] Usuario user)
        {
            userService.InserirUsuario(user);
        }
    }
}
