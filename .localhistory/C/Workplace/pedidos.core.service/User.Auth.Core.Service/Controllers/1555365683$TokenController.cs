﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using User.Auth.Core.Service.Data;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;
        private UserContext context;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
            context = new UserContext();
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult RequestToken([FromBody] Usuario request)
        {


            if (request.nome == "teste" && request.senha == "teste")
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, request.nome),
                };

                var key = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));

                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                        issuer: "iguatemi",
                        audience: "iguatemi",
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(30),
                        signingCredentials: credentials);

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });      
            }
            return BadRequest("Credenciais inválidas...");
        }
    }
}


