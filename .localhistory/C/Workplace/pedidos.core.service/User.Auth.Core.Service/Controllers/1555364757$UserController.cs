﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using User.Auth.Core.Service.Data;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserContext context;

        public UserController()
        {
            context = new UserContext();
        }

        [HttpGet]
        public IEnumerable<Usuario> ChecaUsuario()
        {
            var usuario = context.Usuario.Find(_ => true).ToList();
            return usuario;
        }

        [HttpPost]
        public void InserirUsuario([FromBody] Usuario user)
        {
            context.Usuario.InsertOne(user);
        }

        //// PUT: api/User/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
