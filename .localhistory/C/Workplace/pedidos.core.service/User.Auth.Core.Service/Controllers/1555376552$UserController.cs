﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using User.Auth.Core.Service.Applicaiton;
using User.Auth.Core.Service.Data;
using User.Auth.Core.Service.Models;

namespace User.Auth.Core.Service.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService userService;

        public UserController(IUserService _userService)
        {
            userService = _userService;            
        }

        [HttpGet]
        [Route("PegaUsuario")]
        public IEnumerable<Usuario> PegaUsuario(Usuario user)
        {
            return userService.PegaUsuario(user);
        }

        [HttpGet]
        [Route("ChecaUsuario")]
        public bool ChecaUsuario(Usuario user)
        {
            //var usuario = context.Usuario.Find(x => x.nome == user.nome && x.senha == user.senha).ToList();
            return userService.ChecaUsuario(user);
        }

        [HttpPost]
        [Route("InserirUsuario")]
        public void InserirUsuario([FromBody] Usuario user)
        {
            userService.InserirUsuario(user);
        }
    }
}
